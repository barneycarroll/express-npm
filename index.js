const {lstat, readFile, readPath} = require('fs')
const {promisify} = require('util')
const {join} = require('path')

const zut = async x => {
  throw x
}

const resolve = (root, path) => {
  const endPoint = join(root, path)
  const {isDirectory, isFile} = await promisify(lstat(endPoint))

  return (
     isFile()
    ? endPoint
    : isDirectory()
      ?  
  )
}

export default ({
  root   = '',
  fields = [
    'module',
    'main:next',
    'main',
  ],
  globs  = [
    '*.js',
  ]
}) => {
  const cache = {}  

  return (req, res, next) => {
    
  }
}