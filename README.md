# Importal

## How

In your server code:

```js
import express from 'express'
import importal from 'importal'

express.use(importal('path/to/front-end/entry-point'))
```

You write the entry point thus:

```js
import m from 'mithril'
import app from './app.js'
```

In your public HTML:

```html
<script src=index.js?importal></script>
```

…Importal picks up on the `importal` query and serves the entry point with its import sources re-written to include further `importal` queries:

```js
import m from '?importal=a'
import app from '?importal=b'
```

Because Importal is aware of each source ESM file's position on the back end, it can resolve the import source references and associate them with a hash key. That key is then used as the query value.

## What

Middleware to allow seamless ESM functionality in Node *and* the browser.

In effect, Importal allows you to write front-end ESM code as you normally would - with import sources written such that they resolve using Node - but then serves the code with re-written import sources in such a way that ESM dependencies will work via HTTP in supporting browsers 

## Why 

At the time of conception, ESM works in Node and browsers, but the resolution mechanisms are different: Node's implementation of import resolution is non-deterministic and uses an algorithm that must inherently rely on the file system; in contrast, the browser is not aware of the filesystem — import sources are all taken to be references to ESM resources relative to the base URI of the HTML at the current location.

## Isn't this a solved problem?

The divide between server and browser is essential to the web, and HTTP references cannot work like file system references. The incompatibility between the 2 is a hard boundary and cannot be solved procedurally. Node's module resolution system is non-deterministic. It's fool-hardy to try and bridge that gap while keeping references in the clear, because it exposes the server's inner filesystem details — which is dangerous and in any case unnecessary.

### Right, but haven't people been successfully writing front end code using Node module protocols in the source for years?

Yes! I can't remember the last time I was involved in a web project of any scale that *didn't* use back-end module resolution mechanisms. But all of these — Browserify, Rollup, Webpack — rewrite the source code significantly in order to *emulate* modularity. 